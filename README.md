# MEDIDOR APP#

Aplicación para recibir información de la lectura de los medidores de luz.


## Cual es lo interesante? ##
Este proyecto ha sido modificado para cumplir con el patron de diseño mvp(Model Vista Presentación)

* Fácil para el desarrollo en equipo (Extensible y Mantenible)
* Mejor orden en la distribucion de archivos


### Presentacion ###
Actua entre la vista y el modelo. Recibe datos desde el modelo y retorna formateado a la vista.

### Vista ###
Implementado por un **Activity**(Tambien Fragment, View, etc), contiene una referencia a la capa presentacion. 

### Modelo ###
Es el proveedor de la data que necesitamos mostrar

**FUENTE**
Hay muchos repositorios y con diferentes aplicaciones del concepto MVP, para este proyecto se utilizo el repositorio: [https://github.com/glomadrian/MvpCleanArchitecture](https://github.com/glomadrian/MvpCleanArchitecture)