package monitor.jmt.com.monitorapp.interfaz;

/**
 * Created by JMTech-Android on 20/07/2015.
 */
public interface CallbackRequest {
    void processFinish(boolean b);
    void processFinish(String data);
}
