package monitor.jmt.com.monitorapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;

import monitor.jmt.com.monitorapp.adapter.MedidorAdapter;
import monitor.jmt.com.monitorapp.http.MedidorTask;
import monitor.jmt.com.monitorapp.interfaz.CallbackRequest;
import monitor.jmt.com.monitorapp.interfaz.CbOptionSelected;
import monitor.jmt.com.monitorapp.model.MedidorE;

public class InicioMedidoresActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    ArrayList <MedidorE> lista_medidor;
    Gson gson;
    SwipeRefreshLayout mSwipeRefreshLV;
    private RecyclerView.LayoutManager layoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_medidores);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        obtenerMedidores();
    }

    private void obtenerMedidores() {
        new MedidorTask(this, new CallbackRequest() {
            @Override
            public void processFinish(boolean b) {}
            @Override
            public void processFinish(String data) {
                lista_medidor = gson.fromJson(data, new TypeToken<ArrayList<MedidorE>>(){}.getType());
                listarMedidores();
                if (mSwipeRefreshLV.isRefreshing()) {
                    mSwipeRefreshLV.setRefreshing(false);
                }
            }
        }).execute();
    }

    private void initComponents() {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" "+getString(R.string.s_medidor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_logo_w);
        recyclerView = (RecyclerView) findViewById(R.id.rv_surveys);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        mSwipeRefreshLV= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLV);
        mSwipeRefreshLV.setColorScheme(android.R.color.holo_orange_dark,android.R.color.holo_red_dark,android.R.color.holo_blue_dark,android.R.color.holo_green_dark);
        mSwipeRefreshLV.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                obtenerMedidores();
            }
        });
    }

    private void listarMedidores() {
        adapter = new MedidorAdapter(this,lista_medidor, new CbOptionSelected(){
            @Override
            public void selected(int position) {
                Intent it = new Intent(InicioMedidoresActivity.this,InformacionMedidorActivity.class);
                it.putExtra(S.MEDIDOR.nombre_medidor,lista_medidor.get(position).getNombre());
                it.putExtra(S.MEDIDOR.id_medidor,lista_medidor.get(position).getMedidor_id());
                it.putExtra(S.MEDIDOR.codigo,lista_medidor.get(position).getCodigo());
                it.putExtra(S.MEDIDOR.medidor,lista_medidor.get(position).getDetalle_medidor());
                startActivity(it);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_close) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}