package monitor.jmt.com.monitorapp.http;

import android.content.Context;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import monitor.jmt.com.monitorapp.R;
import monitor.jmt.com.monitorapp.S;
import monitor.jmt.com.monitorapp.model.ResponseE;

public class OkHttp {
    public static String URL="http://192.168.1.100";
    Context ctx;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private final OkHttpClient client = new OkHttpClient();
    public OkHttp(Context ctx){
        this.ctx = ctx;
    }

    public Response makeGetSynchronous(String url) throws IOException {
        Request request = prepare_request(url).build();
        Response r = client.newCall(request).execute();
        return r;
    }
    private Response makePostRequest(String url, String json) throws IOException {
        RequestBody body2 =  RequestBody.create(JSON, json);
        Request request = prepare_request(url).post(body2).build();
        return client.newCall(request).execute();
    }
    private Request.Builder prepare_request(String url){
        return new Request.Builder()
            .url(url);
    }
    private ResponseE showResponse(Response response) {
        ResponseE ss = new ResponseE();
        ss.setMassege("Éxito");
        try {
            String in = response.body().string();
            ss.setSuccess(response.isSuccessful());
            JSONObject jo = new JSONObject(in);
            String code = String.valueOf(response.code());
            ss.setCode(code);
            if(!response.isSuccessful()){
                if(code.equals("500")){
                    ss.setMassege("Error del servidor");
                }else{
                    System.out.println("e.e");
                    System.out.println(jo.toString());
                    ss.setMassege(jo.getString("errors"));
                }
            }else{
                ss.setData(jo.getString("data"));
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            ss.setMassege("Error del servidor, no hay informacion");
            ss.setSuccess(false);return ss;}
        catch (JSONException e) {
            e.printStackTrace();
            ss.setMassege("Error del servidor, intente despues");
            ss.setSuccess(false);return ss;}
        return ss;
    }
    public ResponseE makeGetRequest(String service) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makeGetSynchronous(service);
            return showResponse(r);
        }catch (IOException e){
            e.printStackTrace();
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
    public ResponseE makePostRequest(String service,JSONObject params) {
        String resp = "{}";
        service=URL+service;
        System.out.println(service);
        try {
            Response r = makePostRequest(service,params.toString());
            return showResponse(r);
        }catch (IOException e){
            ResponseE ss = new ResponseE();
            ss.setSuccess(false);
            ss.setCode(S.RESPONSE.without_connection);
            ss.setMassege(ctx.getString(R.string.s_posible_error_de_conexion));
            return ss;
        }
    }
}