package monitor.jmt.com.monitorapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by JMTech-Android on 19/08/2015.
 */
public class DetalleMedidorE implements Serializable {
    @Expose
    @SerializedName("type_id")
    int tipo_medidor;
    @Expose
    @SerializedName("name")
    String nombre;
    @Expose
    @SerializedName("billing_day")
    String fecha_facturacion;
    @Expose
    @SerializedName("active")
    boolean activo;
    @Expose
    @SerializedName("rate")
    double tarifa;
    @Expose
    @SerializedName("measurement")
    double consumo;
    @Expose
    @SerializedName("charged_amount")
    double total;
    @Expose
    @SerializedName("last_measurement_date")
    String ultima_lectura;

    public DetalleMedidorE(String nombre,int drawable,double consumo,double tarifa,String ultima_lectura){
        this.nombre = nombre;
        this.consumo = consumo;
        this.tarifa = tarifa;
        this.ultima_lectura = ultima_lectura;
    }

    public int getTipo_medidor() {
        return tipo_medidor;
    }

    public void setTipo_medidor(int tipo_medidor) {
        this.tipo_medidor = tipo_medidor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha_facturacion() {
        return fecha_facturacion;
    }

    public void setFecha_facturacion(String fecha_facturacion) {
        this.fecha_facturacion = fecha_facturacion;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public double getTarifa() {
        return tarifa;
    }

    public void setTarifa(double tarifa) {
        this.tarifa = tarifa;
    }

    public double getConsumo() {
        return consumo;
    }

    public void setConsumo(double consumo) {
        this.consumo = consumo;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getUltima_lectura() {
        return ultima_lectura;
    }

    public void setUltima_lectura(String ultima_lectura) {
        this.ultima_lectura = ultima_lectura;
    }
}