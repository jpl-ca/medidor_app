package monitor.jmt.com.monitorapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import monitor.jmt.com.monitorapp.HistorialConsumoActivity;
import monitor.jmt.com.monitorapp.R;
import monitor.jmt.com.monitorapp.S;
import monitor.jmt.com.monitorapp.dialog.ActualizarTarifaDialog;
import monitor.jmt.com.monitorapp.http.TarifaTask;
import monitor.jmt.com.monitorapp.interfaz.CallbackRequest;
import monitor.jmt.com.monitorapp.interfaz.CallbackTarifa;
import monitor.jmt.com.monitorapp.interfaz.CbOptionSelected;
import monitor.jmt.com.monitorapp.model.DetalleMedidorE;

public class InfoMedidorAdapter extends RecyclerView.Adapter<InfoMedidorAdapter.ViewHolder> {
    private ArrayList<DetalleMedidorE> ds;
    private Activity ctx;
    private int medidor_id;
    String medidor_code;
    public static CbOptionSelected delegate;
    public InfoMedidorAdapter(Activity _ctx, ArrayList<DetalleMedidorE> dataArgs, CbOptionSelected callback,int _medidor_id,String _medidor_code){
        ds = dataArgs;
        delegate = callback;
        ctx = _ctx;
        medidor_id = _medidor_id;
        medidor_code = _medidor_code;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_info_medidor, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position) {
        holder.position = position;
        final DetalleMedidorE detalle = ds.get(position);
        totalGasto(ds.get(position).getConsumo(),ds.get(position).getTarifa(),holder.tv_gasto);
        holder.tv_consumo.setText(String.valueOf(ds.get(position).getConsumo()));
        holder.tv_ultima_lectura.setText(ds.get(position).getUltima_lectura());
        resizeAppBar(holder.tv_type,holder.tv_img);
        if(position==0){
            holder.iv_consumo.setImageResource(R.mipmap.ic_light);
            holder.tv_img.setImageResource(R.drawable.luz);
            holder.tv_m.setText(" kWh");
            holder.tv_m3.setText("");
            holder.tv_type.setText("LUZ");

        }else{
            holder.iv_consumo.setImageResource(R.mipmap.ic_gota);
            holder.tv_img.setImageResource(R.drawable.agua);
            holder.tv_m.setText(" m");
            holder.tv_type.setText("AGUA");
        }

        holder.iv_chart.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Intent it = new Intent(ctx, HistorialConsumoActivity.class);
                 it.putExtra(S.MEDIDOR.codigo,medidor_code);
                 it.putExtra(S.MEDIDOR.tipo,detalle.getTipo_medidor());
                 ctx.startActivity(it);
             }
         });
        holder.img_config.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ActualizarTarifaDialog(ctx).show(detalle.getTarifa(), new CallbackTarifa() {
                    @Override
                    public void update(String str) {
                        double trf = Double.parseDouble(str);
                        ds.get(position).setTarifa(trf);
                        totalGasto(ds.get(position).getConsumo(),trf,holder.tv_gasto);
                        guardarTarifa(ds.get(position).getTipo_medidor(),trf);
                    }
                });
            }
        });
    }
    public void guardarTarifa(int tipo,double consumo){
        new TarifaTask(ctx, new CallbackRequest() {
            @Override
            public void processFinish(boolean b) {}
            @Override
            public void processFinish(String data) {}
        }).execute(String.valueOf(medidor_id),String.valueOf(tipo),String.valueOf(consumo));
    }

    public void totalGasto(double consumo,double tarifa,TextView tv){
        double gasto = consumo*tarifa;
        tv.setText(new DecimalFormat("0.00").format(gasto));
    }
    @Override
    public int getItemCount() {
        return ds.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView tv_gasto,tv_consumo,tv_ultima_lectura,tv_m,tv_m3,tv_type;
        protected ImageView tv_img,img_config,iv_consumo,iv_chart;
        protected EditText tx;
        protected int position;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_gasto =  (TextView) itemView.findViewById(R.id.tv_gasto);
            tv_consumo =  (TextView) itemView.findViewById(R.id.tv_consumo);
            tv_type =  (TextView) itemView.findViewById(R.id.tv_type);
            tv_ultima_lectura =  (TextView) itemView.findViewById(R.id.tv_ult_hora);
            tv_m3 =  (TextView) itemView.findViewById(R.id.tv_m3);
            tv_m =  (TextView) itemView.findViewById(R.id.tv_m);
            tv_img =  (ImageView) itemView.findViewById(R.id.iv);
            img_config =  (ImageView) itemView.findViewById(R.id.iv_cnfg);
            iv_consumo =  (ImageView) itemView.findViewById(R.id.iv_consumo);
            iv_chart =  (ImageView) itemView.findViewById(R.id.iv_chart);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            delegate.selected(position);
        }
    }

    public DisplayMetrics metricsDevice(){
        return ctx.getResources().getDisplayMetrics();
    }
    public void resizeAppBar(TextView tv_type,ImageView img){
        DisplayMetrics dm = metricsDevice();
        float heightD = dm.heightPixels;
        float widthD = dm.widthPixels;
        int H = (((int)widthD)/4)*3;
        img.getLayoutParams().height = H;

        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(tv_type.getLayoutParams());
        lp.setMargins(50, H-75, 0, 0);
        tv_type.setLayoutParams(lp);
    }
}