package monitor.jmt.com.monitorapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

public class HistorialConsumoActivityDEL extends AppCompatActivity {
    Gson gson;
    LineChartView chart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_consumo_delete);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initComponents() {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(" "+getString(R.string.s_medidor));
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.ic_logo_w);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_close) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
    /**
     * A fragment containing a column chart.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final int DEFAULT_DATA = 0;
        private ColumnChartView chart;
        private ColumnChartData data;
        private boolean hasAxes = true;
        private boolean hasAxesNames = true;
        private boolean hasLabels = false;
        private boolean hasLabelForSelected = false;
        private int dataType = DEFAULT_DATA;

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            setHasOptionsMenu(true);
            View rootView = inflater.inflate(R.layout.fragment_column_chart, container, false);
            chart = (ColumnChartView) rootView.findViewById(R.id.chart);
            chart.setOnValueTouchListener(new ValueTouchListener());
            toggleLabels();
            return rootView;
        }

        private void generateDefaultData() {
            int numSubcolumns = 1;
            int numColumns = 8;
            List<Column> columns = new ArrayList<>();
            List<SubcolumnValue> values;
            int[] val = {85,49,111,87,50,100,161,82};
            for (int i = 0; i < numColumns; ++i) {
                values = new ArrayList<>();
                values.add(new SubcolumnValue((float) val[i], ChartUtils.pickColor()));
                Column column = new Column(values);
                column.setHasLabels(hasLabels);
                column.setHasLabelsOnlyForSelected(hasLabelForSelected);
                columns.add(column);
            }
            data = new ColumnChartData(columns);
            if (hasAxes) {
                List<AxisValue> axisValuesForX = new ArrayList<AxisValue>();
                axisValuesForX.add(new AxisValue((float)0,"ENE".toCharArray()));
                axisValuesForX.add(new AxisValue((float)1,"FEB".toCharArray()));
                axisValuesForX.add(new AxisValue((float)2,"MAR".toCharArray()));
                axisValuesForX.add(new AxisValue((float)3,"ABR".toCharArray()));
                axisValuesForX.add(new AxisValue((float)4,"MAY".toCharArray()));
                axisValuesForX.add(new AxisValue((float)5,"JUN".toCharArray()));
                axisValuesForX.add(new AxisValue((float)6,"JUL".toCharArray()));
                axisValuesForX.add(new AxisValue((float)7,"AGO".toCharArray()));
                Axis axisX = new Axis(axisValuesForX);
                Axis axisY = new Axis().setHasLines(true);
                if (hasAxesNames) {
                    axisX.setName("Mes");
                    axisY.setName("Consumo");
                }
                data.setAxisXBottom(axisX);
                data.setAxisYLeft(axisY);
            } else {
                data.setAxisXBottom(null);
                data.setAxisYLeft(null);
            }
            chart.setColumnChartData(data);
        }

        private void generateData() {
            switch (dataType) {
                case DEFAULT_DATA:
                    generateDefaultData();
                    break;
            }
        }

        private void toggleLabels() {
            hasLabels = !hasLabels;
            if (hasLabels) {
                hasLabelForSelected = false;
                chart.setValueSelectionEnabled(hasLabelForSelected);
            }
            generateData();
        }

        private class ValueTouchListener implements ColumnChartOnValueSelectListener {
            @Override
            public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onValueDeselected() {
            }
        }
    }
}