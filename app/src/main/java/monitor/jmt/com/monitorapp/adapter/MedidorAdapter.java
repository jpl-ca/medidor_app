package monitor.jmt.com.monitorapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import monitor.jmt.com.monitorapp.R;
import monitor.jmt.com.monitorapp.interfaz.CbOptionSelected;
import monitor.jmt.com.monitorapp.model.DetalleMedidorE;
import monitor.jmt.com.monitorapp.model.MedidorE;

public class MedidorAdapter extends RecyclerView.Adapter<MedidorAdapter.ViewHolder> {
    private ArrayList<MedidorE> ds;
    private Context ctx;
    public static CbOptionSelected delegate;
    public MedidorAdapter(Context _ctx, ArrayList<MedidorE> dataArgs, CbOptionSelected callback){
        ds = dataArgs;
        delegate = callback;
        ctx = _ctx;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_medidor, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.position = position;
        holder.tv_nombre_medidor.setText(ds.get(position).getNombre());
        if(position%2==0)holder.iv_medidor.setImageResource(R.drawable.par_def);
        else holder.iv_medidor.setImageResource(R.drawable.impar_def);
        ArrayList<DetalleMedidorE> det_medidor = ds.get(position).getDetalle_medidor();
        if(det_medidor.size()>0&&!det_medidor.get(0).isActivo())holder.iv_foco.setImageResource(R.mipmap.ic_light_w);
        if(det_medidor.size()==2&&!det_medidor.get(1).isActivo())holder.iv_gota.setImageResource(R.mipmap.ic_gota_w);
    }
    @Override
    public int getItemCount() {
        return ds.size();
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        protected TextView tv_nombre_medidor;
        protected ImageView iv_medidor,iv_foco,iv_gota;
        protected int position;
        public ViewHolder(View itemView) {
            super(itemView);
            tv_nombre_medidor =  (TextView) itemView.findViewById(R.id.tv_nombre_medidor);
            iv_medidor =  (ImageView) itemView.findViewById(R.id.iv_medidor);
            iv_foco =  (ImageView) itemView.findViewById(R.id.iv_l);
            iv_gota =  (ImageView) itemView.findViewById(R.id.iv_w);
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            delegate.selected(position);
        }
    }
}