package monitor.jmt.com.monitorapp.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 23/07/2015.
 */
public class PeriodE implements Serializable {
    @Expose
    public String hour_range;
    @Expose
    public String hour;
    @Expose
    public String day;
    @Expose
    public String month;
    @Expose
    public double measurement;
    @Expose
    public double rate;
    @Expose
    public double cost;

    public String getHour_range() {
        return hour_range;
    }

    public void setHour_range(String hour_range) {
        this.hour_range = hour_range;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public double getMeasurement() {
        return measurement;
    }

    public void setMeasurement(double measurement) {
        this.measurement = measurement;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}