package monitor.jmt.com.monitorapp;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import monitor.jmt.com.monitorapp.adapter.InfoMedidorAdapter;
import monitor.jmt.com.monitorapp.interfaz.CbOptionSelected;
import monitor.jmt.com.monitorapp.model.DetalleMedidorE;

public class InformacionMedidorActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    Gson gson;
    ArrayList <DetalleMedidorE> lista_medidor;
    private RecyclerView.LayoutManager layoutManager;
    private String medidor_codigo;
    int id_medidor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_medidores);
        initComponents();
        listarMedidores();
    }
    private void initComponents() {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Bundle extras = getIntent().getExtras();
        lista_medidor = (ArrayList<DetalleMedidorE>) getIntent().getSerializableExtra(S.MEDIDOR.medidor);
        String name = extras.getString(S.MEDIDOR.nombre_medidor);
        id_medidor = extras.getInt(S.MEDIDOR.id_medidor);
        medidor_codigo = extras.getString(S.MEDIDOR.codigo);
        toolbar.setTitle(name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.rv_surveys);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);;
    }
    private void listarMedidores() {
        ArrayList <DetalleMedidorE> lista_medidor_show = new ArrayList<>();
        for (DetalleMedidorE detalle : lista_medidor){
            if(detalle.isActivo())lista_medidor_show.add(detalle);
        }
        adapter = new InfoMedidorAdapter(this,lista_medidor_show, new CbOptionSelected(){
            @Override
            public void selected(int position) {}
        },id_medidor,medidor_codigo);
        recyclerView.setAdapter(adapter);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        if (id == R.id.action_close) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}