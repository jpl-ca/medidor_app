package monitor.jmt.com.monitorapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import monitor.jmt.com.monitorapp.R;
import monitor.jmt.com.monitorapp.S;
import monitor.jmt.com.monitorapp.model.PeriodE;

/**
 * Created by JMTech-Android on 24/08/2015.
 */
public class ReportFragment extends Fragment {
    ArrayList<PeriodE> listR;
    Gson gson;
    private ColumnChartView chart;
    private ColumnChartData data;
    private boolean hasAxes = true;
    private boolean hasAxesNames = true;
    private boolean hasLabels = false;
    private boolean hasLabelForSelected = false;
    int POS = 0,tipoMedidor;
    public static ReportFragment newInstance(int sectionNumber,int tipo,String data) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putInt(S.REPORT.pos, sectionNumber);
        args.putString(S.REPORT.data, data);
        args.putInt(S.MEDIDOR.tipo, tipo);
        fragment.setArguments(args);
        return fragment;
    }

    public ReportFragment() {
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_column_chart, container, false);
        chart = (ColumnChartView) rootView.findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());
        POS = getArguments().getInt(S.REPORT.pos);
        tipoMedidor = getArguments().getInt(S.MEDIDOR.tipo);
        listR = gson.fromJson(getArguments().getString(S.REPORT.data), new TypeToken<ArrayList<PeriodE>>(){}.getType());
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        loadData();
    }

    public void loadData(){
        hasLabels = !hasLabels;
        if (hasLabels) {
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(hasLabelForSelected);
        }
        generateDefaultData();
    }
    private void generateDefaultData() {
        int numColumns = listR.size();
        List<Column> columns = new ArrayList<>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {
            values = new ArrayList<>();
//            values.add(new SubcolumnValue((float) listR.get(i).getMeasurement(), ChartUtils.pickColor()));
            values.add(new SubcolumnValue((float) listR.get(i).getCost(), ChartUtils.pickColor()));
            Column column = new Column(values);
            column.setHasLabels(hasLabels);
            column.setHasLabelsOnlyForSelected(hasLabelForSelected);
            columns.add(column);
        }
        data = new ColumnChartData(columns);
        if (hasAxes) {
            List<AxisValue> axisValuesForX = new ArrayList<>();
            String NameColX = "";
            for (int i = 0; i < numColumns; ++i) {
                String name = "";
                switch (POS){
                    case 0:NameColX = "HORA";name = listR.get(i).getHour();break;
                    case 1:NameColX = "DIA";name = listR.get(i).getDay().substring(8,10);break;
                    case 2:NameColX = "DIA";name = listR.get(i).getDay().substring(8,10);break;
                    case 3:NameColX = "MES";name = listR.get(i).getMonth();break;
                }
                axisValuesForX.add(new AxisValue((float)i,(""+name).toCharArray()));
            }
            Axis axisX = new Axis(axisValuesForX);
            Axis axisY = new Axis().setHasLines(true);
            if (hasAxesNames) {
                axisX.setName(NameColX);
//                axisY.setName(tipoMedidor==1?"Consumo kWh":"Consumo M3");
                axisY.setName("Consumo S/.");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }
        chart.setColumnChartData(data);
    }

    private class ValueTouchListener implements ColumnChartOnValueSelectListener {
        @Override
        public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
//            Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
        }
        @Override
        public void onValueDeselected() {
        }
    }
}