package monitor.jmt.com.monitorapp.http;

import android.app.Activity;
import android.os.AsyncTask;

import monitor.jmt.com.monitorapp.interfaz.CallbackRequest;
import monitor.jmt.com.monitorapp.model.ResponseE;

public class ReporteGraficaTask extends AsyncTask<String, String, ResponseE> {
    Activity ctx;
    CallbackRequest delegate;
    public ReporteGraficaTask(Activity ctx, CallbackRequest delegate){
        this.ctx = ctx;
        this.delegate = delegate;
    }
    @Override
    protected void onPreExecute(){
        super.onPreExecute();
    }
    @Override
    protected ResponseE doInBackground(String... params) {
        ResponseE Z = new ResponseE();
        try {
            Z = new OkHttp(ctx).makeGetRequest("/api/android/reports/all?meter_code="+params[0]+"&measurement_type="+params[1]);
        } catch (Exception e) {e.printStackTrace(); }
        return Z;
    }
    @Override
    protected void onPostExecute(ResponseE response){
        System.out.println("S>>>"+response.isSuccess());
        if(response.isSuccess()){
            delegate.processFinish(response.getData());
        }
        else delegate.processFinish(false);
    }
}