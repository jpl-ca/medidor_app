package monitor.jmt.com.monitorapp;

public final class S {
    public static final class MEDIDOR{
        public static final String nombre_medidor = "nombre_medidor";
        public static final String id_medidor = "id_medidor";
        public static final String medidor = "medidor";
        public static final String codigo = "codigo";
        public static final String tipo = "tipo";
    }
    public static final class RESPONSE {
        public static final String without_connection = "000";
    }
    public static final class REPORT{
        public static final String pos = "pos";
        public static final String data = "data";
    }
}