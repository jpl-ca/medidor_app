package monitor.jmt.com.monitorapp.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import monitor.jmt.com.monitorapp.R;
import monitor.jmt.com.monitorapp.interfaz.CallbackTarifa;

public class ActualizarTarifaDialog {
    AlertDialog.Builder builder;
    Activity ctx;
    public ActualizarTarifaDialog(Activity ctx){
        builder = new AlertDialog.Builder(ctx,R.style.AppCompatAlertDialogStyle);
        this.ctx = ctx;
    }
    public void show(final double trf,final CallbackTarifa delegate){
        LayoutInflater inflater = ctx.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_tarifa, null);
        final EditText txt_tarifa = (EditText)dialogView.findViewById(R.id.txt_tarifa);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        delegate.update(txt_tarifa.getText().toString());
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };
        txt_tarifa.setText(String.valueOf(trf));
        txt_tarifa.setSelection(String.valueOf(trf).length());
        builder.setView(dialogView);
        builder.setTitle("        "+ctx.getString(R.string.s_actualizar_tarifa)+"        ").setPositiveButton("Listo", dialogClickListener)
                .setNegativeButton("Cancelar", dialogClickListener).show();
    }
}