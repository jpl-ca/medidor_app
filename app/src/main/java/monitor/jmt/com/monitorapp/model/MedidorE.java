package monitor.jmt.com.monitorapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by JMTech-Android on 19/08/2015.
 */
public class MedidorE implements Serializable{
    @Expose
    @SerializedName("id")
    int medidor_id;
    @Expose
    @SerializedName("code")
    String codigo;
    @Expose
    @SerializedName("name")
    String nombre;
    @Expose
    @SerializedName("active")
    String activo;
    @Expose
    @SerializedName("measurement_types")
    ArrayList<DetalleMedidorE> detalle_medidor;

    public MedidorE(String nombre){
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getMedidor_id() {
        return medidor_id;
    }

    public void setMedidor_id(int medidor_id) {
        this.medidor_id = medidor_id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getActivo() {
        return activo;
    }

    public void setActivo(String activo) {
        this.activo = activo;
    }

    public ArrayList<DetalleMedidorE> getDetalle_medidor() {
        return detalle_medidor;
    }

    public void setDetalle_medidor(ArrayList<DetalleMedidorE> detalle_medidor) {
        this.detalle_medidor = detalle_medidor;
    }
}