package monitor.jmt.com.monitorapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import monitor.jmt.com.monitorapp.fragment.ReportFragment;
import monitor.jmt.com.monitorapp.http.ReporteGraficaTask;
import monitor.jmt.com.monitorapp.interfaz.CallbackRequest;
import monitor.jmt.com.monitorapp.model.PeriodE;

public class HistorialConsumoActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{
    Gson gson;
    TabLayout tabLayout;
    ViewPager mViewPager;
    ReportFragment fragmentActual;
    int tipo_medidor;
    String codigo_medidor;
    JSONObject joData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_consumo);
        gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        fragmentActual = null;
        Bundle extras = getIntent().getExtras();
        codigo_medidor = extras.getString(S.MEDIDOR.codigo);
        tipo_medidor = extras.getInt(S.MEDIDOR.tipo);
        setupTablayout();
        new ReporteGraficaTask(this, new CallbackRequest() {
            @Override
            public void processFinish(boolean b) {}
            @Override
            public void processFinish(String data) {
                try {
                    joData = new JSONObject(data);
                    setupViewPager();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }).execute(codigo_medidor,String.valueOf(tipo_medidor));
    }
    private void setupViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.pager);
        FragmentPagerAdapter adapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(mViewPager);
    }
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            try {
                String data = "";
                switch (position){
                    case 0:data = joData.getString("day");;break;
                    case 1:data = joData.getString("week");break;
                    case 2:data = joData.getString("month");break;
                    case 3:data = joData.getString("year");break;
                }
                return ReportFragment.newInstance(position,tipo_medidor,data);
            } catch (JSONException e) {
                return null;
            }
        }
        @Override
        public int getCount() {
            return 4;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                    case 0:return getString(R.string.s_DIA);
                    case 1:return getString(R.string.s_SEMANA);
                    case 2:return getString(R.string.s_MES);
                    case 3:return getString(R.string.s_ANIO);
            }
            return null;
        }
    }

    private void setupTablayout(){
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        if(tabLayout == null)return;
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
    @Override
    public void onPageSelected(int position) {}
    @Override
    public void onPageScrollStateChanged(int state) {}
}